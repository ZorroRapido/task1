# Predective analytic project 

House pricing prediction project
For CI/CD concepts demontration

## Installation

```commandline
git clone https://gitlab.com/nicksbor22/task1.git
cd nicksbor22/task1/
python -m venv venv
path to ur venv  /Scripts/activate.ps1
pip install -t requirements.txt
```

## Usage

```commandline
git init
git commit
git pull
git push
```

Run prediction app

```commandline
python run.py
```

## 